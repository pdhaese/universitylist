//
//  Posts.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import Foundation

struct Post {
    var title: String
    var desc: String
    var user: String
    var email: String
}
