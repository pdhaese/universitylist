//
//  Chat.swift
//  WashU List
//
//  Created by Amanda McAdams on 12/3/17.
//
//

import Foundation

struct Chat {
    var email1: String
    var email2: String
    var messages: [Message]
    var key: String
}
