//
//  MessagesViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit
import Firebase

class MessagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var ref: DatabaseReference!
    var user: User!
    var chats: [Chat] = []
    var filteredChats: [Chat] = []
    var itemSelected: Int = 0
    
    @IBOutlet var theTable: UITableView!
    @IBOutlet weak var theSearchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        user = Auth.auth().currentUser
        theTable.dataSource = self
        theTable.delegate = self
        theSearchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getChats()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredChats.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = UITableViewCell(style: .default, reuseIdentifier: "chatCell")
        if(filteredChats[indexPath.row].email1 == user.email!){
            myCell.textLabel?.text = filteredChats[indexPath.row].email2
        } else{
            myCell.textLabel?.text = filteredChats[indexPath.row].email1
        }
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        theSearchBar.resignFirstResponder()
        itemSelected = indexPath.row
        performSegue(withIdentifier: "viewChat", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewChat"{
            let  chatVC:ViewChatViewController = (segue.destination as? ViewChatViewController)!
            chatVC.chat = filteredChats[itemSelected]
        }
    }
    
    @IBAction func newChat(_ sender: Any) {
        performSegue(withIdentifier: "newChat", sender: self)
    }
    
    func getChats() {
        chats = []
        let chatRef = ref.child("chats")
        chatRef.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot] {
                if let dict = child.value as? [String:Any]{
                    let email1 = dict["email1"] as? String
                    let email2 = dict["email2"] as? String
                    let key = child.key
                    if(email1 == self.user.email!) || (email2 == self.user.email!){
                        var messages: [Message] = []
                        chatRef.child(key).child("messages").observeSingleEvent(of: .value, with: { (snapshot) in
                            for child2 in snapshot.children.allObjects as! [DataSnapshot] {
                                if let dict2 = child2.value as? [String:Any]{
                                    let emailFrom = dict2["emailFrom"] as? String
                                    let emailTo = dict2["emailTo"] as? String
                                    let messageText = dict2["message"] as? String
                                    messages.append(Message(emailTo: emailTo!, emailFrom: emailFrom!, messageText: messageText!))
                                }
                            }
                            self.chats.append(Chat(email1: email1!, email2: email2!, messages: messages, key: key))
                            let searchText = self.theSearchBar.text!
                            if searchText != ""{
                                self.filteredChats = self.chats.filter({ (Chat) -> Bool in
                                    var containsSearch = false
                                    if((Chat).email1 == self.user.email){
                                        containsSearch = (Chat).email2.lowercased().contains(searchText.lowercased())
                                    } else{
                                        containsSearch = (Chat).email1.lowercased().contains(searchText.lowercased())
                                    }
                                    return containsSearch
                                })
                            } else{
                                self.filteredChats = self.chats
                            }
                            self.theTable.reloadData()
                        })
                    }
                }
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText != ""){
            filteredChats = chats.filter({ (Chat) -> Bool in
                var containsSearch = false
                if((Chat).email1 == user.email){
                    containsSearch = (Chat).email2.lowercased().contains(searchText.lowercased())
                } else{
                    containsSearch = (Chat).email1.lowercased().contains(searchText.lowercased())
                }
                return containsSearch
            })
        } else{
            filteredChats = chats
        }
        theTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
