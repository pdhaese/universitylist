//
//  MakeChatViewController.swift
//  WashU List
//
//  Created by Nathan Loew on 12/4/17.
//
//

import UIKit
import Firebase

class MakeChatViewController: UIViewController,  UITextFieldDelegate, UITextViewDelegate{
    
    var user: User!
    var ref: DatabaseReference!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var message: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        email.delegate = self
        message.delegate = self
        user = Auth.auth().currentUser
        ref = Database.database().reference()
        setUpTextView()
    }
    
    func setUpTextView() {
        message.layer.borderWidth = 0.5
        message.layer.borderColor = UIColor.lightGray.cgColor
        message.layer.cornerRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func send(_ sender: Any) {
        let key = ref.child("chats").childByAutoId().key
        let chat = ["email1": user.email! as String,
                    "email2": email.text! as String]
        ref.child("chats").child(key).setValue(chat)
        let key2 = ref.child("chats").child(key).child("messages").childByAutoId().key
        let message = ["emailTo": email.text! as String,
                       "emailFrom": user.email! as String,
                       "message": self.message.text! as String]
        ref.child("chats").child(key).child("messages").child(key2).setValue(message)
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email{
            email.resignFirstResponder()
            message.becomeFirstResponder()
        } else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }


}
