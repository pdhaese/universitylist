//
//  AddPostViewController.swift
//  WashU List
//
//  Created by Amanda McAdams on 11/16/17.
//
//

import UIKit
import Firebase

class MakePostViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate{
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    
    var user: User!
    var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleField.delegate = self
        descriptionField.delegate = self
        user = Auth.auth().currentUser
        ref = Database.database().reference()
        setUpTextView()
    }
    
    func setUpTextView() {
        descriptionField.layer.borderWidth = 0.5
        descriptionField.layer.borderColor = UIColor.lightGray.cgColor
        descriptionField.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func post(_ sender: Any) {
        let key = ref.child("posts").childByAutoId().key
        let post = ["user": user.displayName! as String,
                    "email": user.email! as String,
                    "title": titleField.text! as String,
                    "description": descriptionField.text! as String
        ]
        ref.child("posts").child(key).setValue(post)
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == titleField{
            titleField.resignFirstResponder()
            descriptionField.becomeFirstResponder()
        } else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    // hides text views
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    // hides text fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "\n") {
            textField.resignFirstResponder()
            return false
        }
        return true
    }

}
