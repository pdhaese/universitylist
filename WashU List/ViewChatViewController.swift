//
//  ViewChatViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit
import Firebase

class ViewChatViewController : UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    @IBOutlet weak var messageTextView: UITextView!
    var chat: Chat!
    var user: User!
    var ref: DatabaseReference!
    
    @IBOutlet var theTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theTable.dataSource = self
        theTable.delegate = self
        messageTextView.delegate = self
        setUpTextView()
        user = Auth.auth().currentUser
        ref = Database.database().reference()
    }
    
    func setUpTextView() {
        messageTextView.layer.borderWidth = 0.5
        messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        messageTextView.layer.cornerRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chat.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = UITableViewCell(style: .subtitle, reuseIdentifier: "messageCell")
        myCell.textLabel?.text = chat.messages[indexPath.row].emailFrom
        myCell.detailTextLabel?.text = chat.messages[indexPath.row].messageText
        return myCell
    }
    
    @IBAction func send(_ sender: Any) {
        let newMessage = messageTextView.text
        if chat.email1 == user.email{
            chat.messages.append(Message(emailTo: chat.email2, emailFrom: chat.email1, messageText: newMessage!))
            let key = chat.key
            let key2 = ref.child("chats").child(key).child("messages").childByAutoId().key
            let message = ["emailTo": chat.email2 as String,
                           "emailFrom": chat.email1 as String,
                           "message": newMessage!]
            ref.child("chats").child(key).child("messages").child(key2).setValue(message)
        }
        else{
            chat.messages.append(Message(emailTo: chat.email1, emailFrom: chat.email2, messageText: newMessage!))
            let key = chat.key
            let key2 = ref.child("chats").child(key).child("messages").childByAutoId().key
            let message = ["emailTo": chat.email1 as String,
                           "emailFrom": chat.email2 as String,
                           "message": newMessage!]
            ref.child("chats").child(key).child("messages").child(key2).setValue(message)
        }
        theTable.reloadData()
        messageTextView.text = nil
    }
    
    // hides text views
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    // hides text fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "\n") {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
}
