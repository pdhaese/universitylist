//
//  InitialViewController.swift
//  WashU List
//
//  Created by Amanda McAdams on 11/16/17.
//
//

import UIKit
import Firebase

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func signIn(_ sender: Any) {
        performSegue(withIdentifier: "loginPage", sender: self)
    }
    
    @IBAction func signUp(_ sender: Any) {
        performSegue(withIdentifier: "registerPage", sender: self)
    }
}
