
//
//  AccountViewController.swift
//  WashU List
//
//  Created by Amanda McAdams on 11/16/17.
//
//

import UIKit
import Firebase

class AccountViewController: UIViewController, UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var ref: DatabaseReference!
    var user: User!
    var posts: [Post] = []
    var ids: [String] = []
    var itemSelected: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        user = Auth.auth().currentUser
        ref = Database.database().reference()
        emailLabel.text = user.displayName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPosts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myPostCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "myPostCell")
        cell.textLabel!.text = posts[indexPath.row].title
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelected = indexPath.row
        performSegue(withIdentifier: "toMyPost", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMyPost"{
            let  postVC:MyPostViewController = (segue.destination as? MyPostViewController)!
            postVC.post = posts[itemSelected]
            postVC.postID = ids[itemSelected]
        }
    }
    
    func getPosts() {
        posts = []
        ids = []
        let postRef = ref.child("posts")
        postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot] {
                if let dict = child.value as? [String:Any]{
                    let email = dict["email"] as? String
                    if email == self.user.email{
                        self.ids.append(child.key)
                        let title = dict["title"] as? String
                        let desc = dict["description"] as? String
                        let name = dict["user"] as? String
                        self.posts.append(Post(title: title!, desc: desc!, user: name!, email: email!))
                    }
                }
            }
            self.tableView.reloadData()
        })
    }

    
    @IBAction func logout(_ sender: Any) {
        try! Auth.auth().signOut()
        performSegue(withIdentifier: "logout", sender: self)
    }

    

}
