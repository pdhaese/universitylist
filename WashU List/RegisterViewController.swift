//
//  RegisterViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        passwordField.delegate = self
        firstNameField.delegate = self
        lastNameField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func register(_ sender: Any) {
        let name = firstNameField.text! + " " + lastNameField.text!
        let email = emailField.text!
        let password = passwordField.text!
        let emailRegEx = "[A-Z0-9a-z._%+-]+@wustl.edu"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: email){
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if let error = error {
                    self.message.text = (error.localizedDescription)
                    return
                }
                //Auth.auth().currentUser?.sendEmailVerification { (error) in
                  //  if let error = error {
                    //    self.message.text = (error.localizedDescription)
                      //  return
                    //}
                    self.emailField.text = nil
                    self.passwordField.text = nil
                    self.firstNameField.text = nil
                    self.lastNameField.text = nil
                    self.message.text = "Account created. You may now log in."
                //}
                if let user = Auth.auth().currentUser {
                    let changeRequest = user.createProfileChangeRequest()
                    changeRequest.displayName = name
                    changeRequest.commitChanges { error in
                        if error != nil {
                            // An error happened.
                        } else {
                            // Profile updated.
                        }
                    }
                }
            }
        }
        else{
            message.text = "Must use a wustl.edu email."
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === firstNameField){
            firstNameField.resignFirstResponder()
            lastNameField.becomeFirstResponder()
        }
        else if (textField === lastNameField){
            lastNameField.resignFirstResponder()
            emailField.becomeFirstResponder()
        }
        else if (textField === emailField){
            emailField.resignFirstResponder()
            passwordField.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    
}
