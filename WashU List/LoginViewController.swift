//
//  LoginViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var message: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        passwordField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: Any) {
        let email = emailField.text!
        let password = passwordField.text!
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                self.message.text = (error.localizedDescription)
                return
            }
            //if let user = Auth.auth().currentUser{
                //if user.isEmailVerified{
                    self.emailField.text = nil
                    self.passwordField.text = nil
                    self.performSegue(withIdentifier: "login", sender: self)
                //} else {
                  //  self.message.text = "Account has not been verified."
                //}
            //}
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === emailField){
            emailField.resignFirstResponder()
            passwordField.becomeFirstResponder()
        } else{
            passwordField.resignFirstResponder()
        }
        return true
    }
    
    
}
