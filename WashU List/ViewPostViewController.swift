//
//  ViewPostViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit

class ViewPostViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var post: Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = post.title
        nameLabel.text = post.user
        emailLabel.text = post.email
        descriptionTextView.text = post.desc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    
    
}
