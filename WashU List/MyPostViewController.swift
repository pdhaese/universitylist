//
//  MyPostViewController.swift
//  WashU List
//
//  Created by Amanda McAdams on 11/16/17.
//
//

import UIKit
import Firebase

class MyPostViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate{
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    
    var post: Post!
    var postID: String = ""
    var user: User!
    var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleField.delegate = self
        descriptionField.delegate = self
        user = Auth.auth().currentUser
        ref = Database.database().reference()
        setUpTextView()
        titleField.text = post.title
        descriptionField.text = post.desc
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpTextView() {
        descriptionField.layer.borderWidth = 0.5
        descriptionField.layer.borderColor = UIColor.lightGray.cgColor
        descriptionField.layer.cornerRadius = 5
    }
    
    @IBAction func save(_ sender: Any) {
        let post = ["user": user.displayName! as String,
                    "email": user.email! as String,
                    "title": titleField.text! as String,
                    "description": descriptionField.text! as String
        ]
        ref.child("posts").child(postID).setValue(post)
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deletePost(_ sender: Any) {
        ref.child("posts").child(postID).removeValue()
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == titleField{
            titleField.resignFirstResponder()
            descriptionField.becomeFirstResponder()
        } else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    // hides text views
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    // hides text fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "\n") {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}
