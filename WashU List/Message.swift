//
//  Message.swift
//  WashU List
//
//  Created by Nathan Loew on 12/4/17.
//
//

import Foundation

struct Message {
    var emailTo: String
    var emailFrom: String
    var messageText: String
}
