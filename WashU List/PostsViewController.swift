//
//  PostsViewController.swift
//  wustlcraigslist
//
//  Created by labuser on 11/16/17.
//  Copyright © 2017 wustl. All rights reserved.
//

import UIKit
import Firebase

class PostsViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate{
    var ref: DatabaseReference!
    var posts: [Post] = []
    var filteredPosts: [Post] = []
    var itemSelected: Int = 0
    
    @IBOutlet var theTable: UITableView!
    @IBOutlet weak var theSearchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        theSearchBar.delegate = self
        ref = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPosts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupTableView() {
        theTable.dataSource = self
        theTable.delegate = self
        theTable.register(UITableViewCell.self, forCellReuseIdentifier: "postCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPosts.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "postCell")
        cell.textLabel!.text = filteredPosts[indexPath.row].title
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        theSearchBar.resignFirstResponder()
        itemSelected = indexPath.row
        performSegue(withIdentifier: "toPost", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPost"{
            let  postVC:ViewPostViewController = (segue.destination as? ViewPostViewController)!
            postVC.post = filteredPosts[itemSelected]
        }
    }
    
    @IBAction func makePost(_ sender: Any) {
        performSegue(withIdentifier: "makePost", sender: self)
    }
    
    func getPosts() {
        posts = []
        let postRef = ref.child("posts")
        postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot] {
                if let dict = child.value as? [String:Any]{
                    let title = dict["title"] as? String
                    let desc = dict["description"] as? String
                    let name = dict["user"] as? String
                    let email = dict["email"] as? String
                    self.posts.append(Post(title: title!, desc: desc!, user: name!, email: email!))
                }
            }
            let searchText = self.theSearchBar.text
            if searchText != ""{
                self.filteredPosts = self.posts.filter({ (Post) -> Bool in
                    (Post).title.lowercased().contains(searchText!.lowercased())
                })
            } else{
                self.filteredPosts = self.posts
            }
            self.theTable.reloadData()
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText != ""){
            filteredPosts = posts.filter({ (Post) -> Bool in
                (Post).title.lowercased().contains(searchText.lowercased())
            })
        } else{
            filteredPosts = posts
        }
        theTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
}
